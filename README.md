# ZWI Format Specification, version 1.3 (WIP)
**ARCHIVED: Moved to https://docs.encyclosphere.org**


**Note:** This is a first draft. Some information may be inaccurate or incorrect.

## What is a ZWI file?
ZWI files are used to publish and transmit encyclopedia articles. ZWI is pronounced "zwee."

The ZWI format was created by [Sergei Chekanov](http://jwork.org/chekanov/). This specification was created by [Henry Sanger](https://henry.sanger.io/blog).

## Anatomy of a ZWI file
ZWI files are ZIP files with a different extension, so each ZWI file is actually made up of multiple files. Here's the contents of an example ZWI file, which you can download [here](url):
- `data`
    - `css`
        - `main.css`
        - `theme.css`
    - `media`
        - `images`
            - `firstimage.png`
            - `SecondImage.jpg`
            - `third_image.gif`
            - `video-preview-1.jpg`
            - `video-preview-2.jpg`
            - `video-preview-3.jpg`
        - `videos`
            - `first-video.mp4`
            - `SecondVideo.webm`
            - `third_video.mp4`
- `article.html`
- `article.md`
- `article.txt`
- `article.wikitext`
- `media.json`
- `metadata.json`

The only file that's required is `metadata.json`. CSS files must be in `data/css`, images must be in `data/media/images`, videos must be in `data/media/videos`, audio must be in `data/media/audio`.

The ZWI file itself can have any name. ZWI files outside of a database usually have the article title as their filename, while files inside of a database must have the ID as their filename.

### The ID system
Every ZWI file has a unique ID. This ID is the SHA-256 hash of the source URL, with trailing slashes (slashes at the end of the URL) removed.

### `metadata.json`
This is the most important file. Here's the `metadata.json` from the example article I showed above:
```json
{
    "ZWIversion": 1.3,
    "Title": "Example_article",
    "Lang": "en",
    "Content": {
        "article.html": "d5a077655c7b4049404210d473463a33d0d79378",
        "article.md": "3777ffb5c35550195074ae29c02c058f0876ef86",
        "article.wikitext": "99270e7fcdba7eaa9e8868e89810e11da4047eb0",
        "article.txt": "027f9ec9b7a4275d6e3cd1508909bc2b198d086a"
    },
    "Primary": "article.html",
    "Revisions": [],
    "Publisher": "Example Encyclopedia",
    "CreatorNames": [
        "username1",
        "Another_Username"
    ],
    "ContributorNames": [
        "Contributor1",
        "contributor_2"
    ],
    "LastModified": "499138500",
    "TimeCreated": "499138500",
    "Categories": [
        "Example articles"
    ],
    "Topics": [],
    "Rating": [],
    "Description": "An example article to explain the ZWI format",
    "Comment": "Additional information goes here",
    "License": "CC BY-SA 3.0",
    "SourceURL": "https://example.com/wiki/Example_article"
}
```

Property | Explanation
--- | ---
`ZWIversion`       | The version of the ZWI specification this article adheres to (currently 1.3)
`Title`            | The title of the article
`Lang`             | The [ISO 639-1](https://www.iso.org/standard/22109.html) 2-letter language code representing the language the article is in
`Content`          | A list of article files and their SHA-1 hashes, in the same format as `media.json`
`Primary`          | The main/preferred article file. If there are no article files, this field should be left blank
`Revisions`        | A list of revisions
`Publisher`        | The source that published the article
`CreatorNames`     | A list of people that created the article. Can be real names or usernames
`ContributorNames` | A list of people that worked on the article
`LastModified`     | The number of seconds that had passed since the Unix epoch (January 1, 1970) when the article was last modified
`TimeCreated`      | When the article was created, in the same format as `LastModified`
`Categories`       | A list of categories
`Topics`           | A list of topics
`Rating`           | Currently unused. In the future, this property will be used to implement a rating system.
`Description`      | A short description of the article, or the first few sentences of the article
`Comment`          | Additional information about the article
`License`          | The article's license
`SourceURL`        | A URL that points to the source of the article

### `media.json`
This file contains the names of all of the files in the `data` folder, and their [SHA-1 hashes](https://encycloreader.org/find.php?query=sha-1+hash). Example:
```json
{
	"data/css/main.css": "d4a02937e87bf586a5981195165e8dbb6fc39e99",
	"data/css/theme.css": "810626075100cc95bb40be14142959919571ee98",
	"data/media/images/firstimage.png": "18cdf3fe481f8a80d31dd63a27b9d21f39cf4f8f",
	"data/media/images/SecondImage.jpg": "8741692c763a3ef2592f9b096e451c9ad5823f26",
	"data/media/images/third_image.gif": "4cdeaea64188611663cc3b01bfd2a67671b06dc6",
	"data/media/images/video-preview-1.jpg": "12aa82f74e0405329d742f8f0779d1e2c094eaf5",
	"data/media/images/video-preview-2.jpg": "af9e35aa159cdd5e5dd30c8811a7ce4cffd6e85f",
	"data/media/images/video-preview-3.jpg": "d7fac6e1b41a103c236e458b87672fffc9a361bb",
	"data/media/videos/first-video.mp4": "4a7ad9c486dc6b3f0c13e5c89a39d5ec239a7063",
	"data/media/videos/SecondVideo.webm": "df50368d447824980f31fe4ccf9c36da3f7dde81",
	"data/media/videos/third_video.mp4": "0dd6b6fd8f1e42bf57c6198741222d96ad2933ce"
}
```

If there is no `data` folder in the ZWI file, this file can be left out.

### The `data` folder
This folder contains assets required by article files. Images go in `data/media/images`, and videos go in `data/media/videos`. CSS for `article.html` goes in `data/css`.
- `data`
    - `css`
        - `main.css`
        - `theme.css`
    - `media`
        - `images`
            - `firstimage.png`
            - `SecondImage.jpg`
            - `third_image.gif`
        - `videos`
            - `first-video.mp4`
            - `SecondVideo.webm`
            - `third_video.mp4`

This folder is optional. If article files don't require any images, videos, or CSS, it can be left out.

### Article files
These files contain the content of the article. The content can be in any format, but the filename must be `article`. Articles generated by EncycloReader always contain `article.html`, `article.txt`, and `article.wikitext`.

Correct:
- `article.txt`
- `article.md`

Incorrect:
- `my_article.txt`
- `NewArticle.article`
